﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace HotelBookings.Core
{
    internal class BookedDates
    {
        internal readonly ConcurrentBag<DateTime> Dates;

        public BookedDates(DateTime date)
        {
            Dates = new ConcurrentBag<DateTime>
            {
                date
            };
        }

        internal void AddDate(DateTime date)
        {
            Dates.Add(date);
        }

        internal int GetCount()
        {
            return Dates.Count;
        }

        internal bool IsEmpty(DateTime date)
        {
            return Dates.IsEmpty || !Contains(date);
        }

        internal bool Contains(DateTime date)
        {
            return Dates.Contains(date);
        }
    }
}
